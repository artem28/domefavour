# README #

This is the web application which should help users to share its impressions about particular places on a global map,  ask questions, find out interesting places or help somebody answering questions. So it is all about publishing some information on a map. Currently are supported only text and images content types.  

### Details ###

* The deployed application is available here [http://www.domefavour.info](https://www.domefavour.info) 
* You can use Twitter , Facebook or Google accounts to log in
* There is also [Android application](https://bitbucket.org/artem28/android-geocomm-application) is developed in connection to this site