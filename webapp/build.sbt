import com.github.play2war.plugin._

name := "webapp"

version := "1.0"

lazy val `webapp` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

resolvers := ("Atlassian Releases" at "https://maven.atlassian.com/public/") +: resolvers.value

resolvers += Resolver.url("Typesafe Ivy releases", url("https://repo.typesafe.com/typesafe/ivy-releases"))(Resolver.ivyStylePatterns)

//resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

//resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq( jdbc , anorm , cache , ws,
  "com.mohiva" %% "play-silhouette" % "2.0.1",
  "net.codingwell" %% "scala-guice" % "4.0.0-beta5",
  "com.google.inject" % "guice" % "4.0",
  "javax.inject" % "javax.inject" % "1",
  "org.reactivemongo" %% "play2-reactivemongo" % "0.11.7.play23",
  "org.webjars" % "bootstrap" % "3.3.4",
  "org.webjars" % "angularjs" % "1.3.15",
  "org.webjars" % "angular-ui-bootstrap" % "0.13.0",
  "org.mockito" % "mockito-core" % "1.10.19" % "test",
  "com.google.code.gson" % "gson" % "2.2",
  "org.imgscalr" % "imgscalr-lib" % "4.2"
)

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

Play2WarPlugin.play2WarSettings

Play2WarKeys.servletVersion := "2.5"

