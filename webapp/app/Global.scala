import com.google.inject.{AbstractModule, Guice}
import com.mohiva.play.silhouette.api.{Logger, SecuredSettings}
import filters.HTTPSRedirectFilter
import org.slf4j.LoggerFactory
import play.Application
import play.api.GlobalSettings
import play.api.mvc._
//import services.{SimpleUUIDGenerator, UUIDGenerator}
import utils.di.SilhouetteModule

/**
 * Set up the Guice injector and provide the mechanism for return objects from the dependency graph.
 */

object Global extends GlobalSettings  with SecuredSettings with Logger{

  private final val loggerMy: org.slf4j.Logger = LoggerFactory.getLogger(classOf[GlobalSettings])

  override def doFilter(next: EssentialAction): EssentialAction = {
    Filters(super.doFilter(next), HTTPSRedirectFilter)
  }

  /**
   * Bind types such that whenever UUIDGenerator is required, an instance of SimpleUUIDGenerator will be used.
   */
  val injector = Guice.createInjector(new SilhouetteModule )

//


  /**
   * Controllers must be resolved through the application context. There is a special method of GlobalSettings
   * that we can override to resolve a given controller. This resolution is required by the Play router.
   */
  override def getControllerInstance[A](controllerClass: Class[A]) = injector.getInstance(controllerClass)

}
