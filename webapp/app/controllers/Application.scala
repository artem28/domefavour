package controllers

import java.io.File
import javax.inject.{Inject, Singleton}

import akka.actor.FSM.Failure
import akka.actor.Status.Success
import com.mohiva.play.silhouette.api.{Environment, Silhouette}
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import models.{Question, User}
import play.api.Play
import play.api.Play.current
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.Try

//import services.UUIDGenerator
import org.slf4j.{LoggerFactory, Logger}
import play.api.mvc._

/**
 * Instead of declaring an object of Application as per the template project, we must declare a class given that
 * the application context is going to be responsible for creating it and wiring it up with the UUID generator service.
 *
 */
@Singleton
class Application @Inject() (implicit val env: Environment[User, JWTAuthenticator], questionsController: Questions)
  extends Silhouette[User, JWTAuthenticator] {

    private final val loggerMy: Logger = LoggerFactory.getLogger(classOf[Application])

  def index = Action { implicit request =>
    loggerMy.info("Serving index page...")

    val javascripts = {
      if (Play.isDev) {
        // Load all .js and .coffeescript files within app/assets
        Option(Play.getFile("app/assets")).
          filter(_.exists).
          map(findScripts).
          getOrElse(Nil)
      } else {
        // Concated and minified by UglifyJS
        "concat.min.js" :: Nil
      }
    }
    var question : Option[Question] = None
    request.getQueryString("qid") match  {
      case Some(qid) =>

          val result : Try[List[Question]] = Try({
            val f: Future[List[Question]] = questionsController.findQuestionInt(qid.trim)
            val qList: List[Question] = Await.result(f, Duration.Inf)
            qList
          })
          result match {
            case scala.util.Success(qList) =>
              if (qList.nonEmpty) {
                question = Option(qList.head)
              }
            case scala.util.Failure(f) =>
              loggerMy.warn(s"Cannot fetch question from request parameter: $qid", f)
          }
      case _ =>

    }

    Ok(views.html.index(question, javascripts, env))
  }

  private def findScripts(base: File): Seq[String] = {
    val baseUri = base.toURI
    directoryFlatMap(base, scriptMapper).
      map(f => baseUri.relativize(f.toURI).getPath)
  }

  private def scriptMapper(file: File): Option[File] = {
    val name = file.getName
    if (name.endsWith(".js")) Some(file)
    else if (name.endsWith(".coffee")) Some(new File(file.getParent, name.dropRight(6) + "js"))
    else None
  }

  private def directoryFlatMap[A](in: File, fileFun: File => Option[A]): Seq[A] = {
    in.listFiles.flatMap {
      case f if f.isDirectory => directoryFlatMap(f, fileFun)
      case f if f.isFile => fileFun(f)
    }
  }

  /**
   * Returns the user.
   *
   * @return The result to display.
   */
  def user = SecuredAction.async { implicit request =>
    Future.successful(
      Ok(
        Json.toJson(
          request.identity)))
  }


  /* def randomUUID = Action {
     loggerMy.info("calling UUIDGenerator...")
     Ok(uuidGenerator.generate.toString)
   }*/

}
