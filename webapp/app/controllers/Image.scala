package controllers

import java.io.InputStream
import javax.inject.Singleton

import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.iteratee.Enumerator
import play.api.libs.json.Json
import play.api.libs.ws.WS
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import services.MFUtils

import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global
/**
 * The Image controllers encapsulates the Rest endpoints and the interaction with the MongoDB, via ReactiveMongo
 * play plugin. This provides a non-blocking driver for mongoDB as well as some useful additions for handling JSon.
  *
  * @see https://github.com/ReactiveMongo/Play-ReactiveMongo
 */
@Singleton
class Image extends Controller with MongoController {

  private final val logger: Logger = LoggerFactory.getLogger(classOf[Image])


  /*
   * Get a JSONCollection (a Collection implementation that is designed to work
   * with JsObject, Reads and Writes.)
   * Note that the `collection` is not a `val`, but a `def`. We do _not_ store
   * the collection reference to avoid potential problems in development with
   * Play hot-reloading.
   */
  def collection: JSONCollection = db.collection[JSONCollection]("questions")

  def iUtils = new MFUtils()

  // ------------------------------------------ //
  // Using case classes + Json Writes and Reads //
  // ------------------------------------------ //

  /*
    def upload = Action(parse.temporaryFile) { request =>
  //    request.body.moveTo(new File("/home/artem/projects/scala/domefavor/webapp"))
      val fileId = iUtils.uploadFromStream(request.body.file)
      Ok(fileId)
    }
  */


  def upload = Action(parse.multipartFormData) { request =>
    request.body.file("file").map { picture =>
      val filename = picture.filename
      val contentType = picture.contentType
      val size = picture.ref.file.length()
      if(size > 1024000) {
        logger.warn(s"The image was not uploaded $filename  as too HUGE!!!")
        new Status(METHOD_NOT_ALLOWED)
      } else {
        val (fileId, thumbId) = iUtils.uploadFromStream(picture.ref.file)
        logger.info(s"The image was uploaded $filename with id $fileId  and thumnailId $thumbId")
        Ok(Json.toJson(Map("fileId" -> fileId.toString, "thumbId" -> thumbId.toString, "fileName" -> filename.toString, "contentType" -> contentType.toString)))
      }
    }.getOrElse {
      Redirect(routes.Application.index).flashing(
        "error" -> "Missing file"
      )
    }
  }

  def get (quickKey :String ) = Action.async {
    val resultFuture = WS.url(iUtils.getDirectUrl(quickKey)).getStream()
    val dataEnumeratorFuture = resultFuture.map(stream => stream._2)
    dataEnumeratorFuture.map(Ok.chunked(_).as("image/png"))
  /*
    WS.url(iUtils.getDirectUrl(qid)).get().map { response =>
    val asStream: InputStream = response.getAHCResponse.getResponseBodyAsStream
    Ok.chunked(Enumerator.fromStream(asStream)).as("image/png")
    }*/
  }

}


