package controllers

import java.util.{Calendar, Date}
import javax.inject.{Inject, Singleton}

import services.UserActionService
import com.mohiva.play.silhouette.api.{Environment, LoginInfo, Silhouette}
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import models.{User, services, _}
import models.daos.UserDAO
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json._
import play.modules.reactivemongo.json.collection.{JSONCollection, _}
import reactivemongo.api.Cursor

import scala.concurrent.Future

/**
 * The Feedbacks controllers encapsulates the Rest endpoints and the interaction with the MongoDB, via ReactiveMongo
 * play plugin. This provides a non-blocking driver for mongoDB as well as some useful additions for handling JSon.
 * @see https://github.com/ReactiveMongo/Play-ReactiveMongo
 */
@Singleton
class Feedbacks @Inject()(implicit val env: Environment[User, JWTAuthenticator], userDAO: UserDAO,
                          questionsController: Questions,  actionService : UserActionService)
  extends Silhouette[User, JWTAuthenticator] with MongoController {

  private final val logger2: Logger = LoggerFactory.getLogger(classOf[Feedbacks])

  /*
   * Get a JSONCollection (a Collection implementation that is designed to work
   * with JsObject, Reads and Writes.)
   * Note that the `collection` is not a `val`, but a `def`. We do _not_ store
   * the collection reference to avoid potential problems in development with
   * Play hot-reloading.
   */
  private def collection: JSONCollection = db.collection[JSONCollection]("feedbacks")

  private def questions: JSONCollection = db.collection[JSONCollection]("questions")

  // ------------------------------------------ //
  // Using case classes + Json Writes and Reads //
  // ------------------------------------------ //


  private def changeQuestionRating(qid: String, positive: Boolean) = {
    logger2.info(s"changeQuestionRating requested with param: id = $qid")
    val selector = Json.obj("qid" -> qid)
    val update = Json.obj("$inc" -> Json.obj((if (positive) "rating" else "badRating") -> 1),
      "$inc" -> Json.obj("rel" -> (if (positive) 1 else -1)))
    questions.update(selector, update).map {
      lastError =>
        logger2.info(s"Successfully updated question $qid with positive $positive with LastError: $lastError")
    }
  }

  private def changeQuestionRating(qid: String, aid: String, positive: Boolean) = {
    logger2.info(s"changeQuestionRating requested with param: id = $qid aid=$aid")

    val selector = Json.obj("qid" -> qid, "answers.aid" -> aid)
    val update = Json.obj("$inc" -> Json.obj((if (positive) "answers.$.rating" else "answers.$.badRating") -> 1),
      "$inc" -> Json.obj("rel" -> (if (positive) 1 else -1)))
    questions.update(selector, update).map {
      lastError =>
        logger2.info(s"Successfully updated question $qid , answer $aid with positive $positive with LastError: $lastError")
    }
  }

  def positiveFeedback = setFeedback(true)

  def negativeFeedback = setFeedback(false)

  private def setFeedback(positive: Boolean) = SecuredAction.async(parse.json) {
    request =>

      request.body.validate[Feedback].map {
        feedback =>

          //`feedback` is an instance of the case class `models.Feedback`
          feedback.created = Calendar.getInstance().getTime
          feedback.loginInfo = Some(request.identity.loginInfo)
          val selector = Json.obj("loginInfo" -> feedback.loginInfo, "qid" -> feedback.qid)

          findChangedRating(feedback)
          collection.update(selector, feedback, upsert = true).map {
            lastError =>
              //changeQuestionRating(feedback.qid, positive)
              logger.info(s"Successfully feedback was set with LastError: $lastError")
              Ok
          }
      }.getOrElse(Future.successful(BadRequest("invalid feedback json")))
  }

  private def findUserAndQuestion(questionsFuture: Future[Question], aid: String, positive: Boolean): Unit = {
    questionsFuture.map { q: Question =>
      val answerOption: Option[Answer] = q.answers.find { a: Answer => a.aid == aid }
      if (answerOption.nonEmpty) {
        updateUser(answerOption.get.user, positive)
        changeQuestionRating(q.qid, aid, positive)
        actionService.saveAction(new UserAction(q.user,
          ActionType.Like, Option(q.qid), Option(aid), Calendar.getInstance().getTime))
      }
    }
  }

  private def updateUser(loginInfo: LoginInfo, positive: Boolean): Unit = {
    userDAO.find(loginInfo).map { user: Option[User] => if (user.nonEmpty) {
      if (positive) {
        user.get.rating +=1
      } else {
        user.get.badRating +=1
      }
      userDAO.save(user.get)
    }}
  }

  private def findUserAndQuestion(questionsFuture: Future[Question], positive: Boolean): Unit = {
    questionsFuture.map { q: Question =>
      updateUser(q.user, positive)
      changeQuestionRating(q.qid, positive)
      actionService.saveAction(new UserAction(q.user,
        ActionType.Like, Option(q.qid), None, Calendar.getInstance().getTime))
    }
  }

  private def getQuestion(qid: String): Future[Question] = {
    questionsController.findQuestionInt(qid).map{ q => q.head}
  }

  private def findChangedRating(feedback: Feedback): Unit = {
    val questionFuture: Future[Question] = getQuestion(feedback.qid)
    val oldFeedbackFuture: Future[Option[Feedback]] = getFeedbackInt(feedback.qid, feedback.loginInfo.get)
    var result :String = feedback.qid
    oldFeedbackFuture.map { oldFeedbackOption: Option[Feedback] =>
      oldFeedbackOption match {
        case None =>
          if(feedback.answers.nonEmpty){
            val answerFeedback: Option[(String, Boolean)] = feedback.answers.headOption
            if (answerFeedback.nonEmpty) {
              findUserAndQuestion(questionFuture, answerFeedback.get._1, answerFeedback.get._2)
            }
          } else {
            findUserAndQuestion(questionFuture, feedback.positive.getOrElse(true))
          }
        case Some(oldFeedback) =>
          oldFeedback.positive match {
            case feedback.positive =>
              val answerFeedback: Option[(String, Boolean)] = feedback.answers.filterKeys { key: String => !oldFeedback.answers.contains(key) }.headOption
              if (answerFeedback.nonEmpty) {
                findUserAndQuestion(questionFuture, answerFeedback.get._1, answerFeedback.get._2)
              }
            case _ =>
              findUserAndQuestion(questionFuture, feedback.positive.getOrElse(true))
          }
      }
    }
  }

  private def getFeedbackInt(qid: String, loginInfo: LoginInfo): Future[Option[Feedback]] = {
    val query: JsObject = Json.obj("qid" -> qid, "loginInfo" -> loginInfo)

    // let's do our query
    val cursor: Cursor[Feedback] = collection.
      // find all
      find(query).
      sort(Json.obj("created" -> -1)).
      // perform the query and get a cursor of JsObject
      cursor[Feedback]

    // gather all the JsObjects in a list
    val futureFeedbacksList: Future[List[Feedback]] = cursor.collect[List](1, false)

    // transform the list into a JsArray
    val result: Future[Option[Feedback]] = futureFeedbacksList.map { feedbacks: List[Feedback] =>
      feedbacks.headOption
    }
    result
  }

  def getFeedback(qid: String) = SecuredAction.async { implicit request =>
    logger2.info(s"getFeedback requested with params: qid = $qid")

    // transform the list into a JsArray
    val futureFeedback: Future[Option[Feedback]] = getFeedbackInt(qid, request.identity.loginInfo)

    // everything's ok! Let's reply with the array
    futureFeedback.map {
      feedbackOption =>
        feedbackOption match {
          case None =>
            Ok(JsNull)
          case Some(feedback) =>
            feedback.loginInfo = Some(OWNERUSER)
            Ok(Json.toJson(feedback))
        }
    }
  }

  val OWNERUSER: LoginInfo = LoginInfo("owner", "owner")
}
