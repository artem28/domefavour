package controllers

import java.time.temporal.ChronoUnit
import java.time.{LocalDate, Period}
import java.util.{Calendar, UUID}
import javax.inject.{Inject, Singleton}

import com.google.gson.JsonNull
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator
import models.User
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.JsValueWrapper
import play.api.libs.json._
import play.api.mvc._
import com.mohiva.play.silhouette.api.{Environment, LoginInfo, Silhouette}
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json._
import play.modules.reactivemongo.json.collection.{JSONCollection, _}
import reactivemongo.api.Cursor
import reactivemongo.api.commands.{UpdateWriteResult, WriteResult}
import services.UserActionService

import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

/**
  * The Questions controllers encapsulates the Rest endpoints and the interaction with the MongoDB, via ReactiveMongo
  * play plugin. This provides a non-blocking driver for mongoDB as well as some useful additions for handling JSon.
  *
  * @see https://github.com/ReactiveMongo/Play-ReactiveMongo
  */
@Singleton
class Questions @Inject()(implicit val env: Environment[User, JWTAuthenticator], actionService: UserActionService)
  extends Silhouette[User, JWTAuthenticator] with MongoController {

  private final val logger2: Logger = LoggerFactory.getLogger(classOf[Questions])
  private final val STARTDATE: LocalDate = LocalDate.of(2001, 1, 1)

  /*
   * Get a JSONCollection (a Collection implementation that is designed to work
   * with JsObject, Reads and Writes.)
   * Note that the `collection` is not a `val`, but a `def`. We do _not_ store
   * the collection reference to avoid potential problems in development with
   * Play hot-reloading.
   */
  def collection: JSONCollection = db.collection[JSONCollection]("questions")

  // ------------------------------------------ //
  // Using case classes + Json Writes and Reads //
  // ------------------------------------------ //

  import models.JsonQuestionFormats._
  import models.JsonAnswerFormats.answerFormat
  import models.JsonFileFormats._
  import models._

  def createQuestion = SecuredAction.async(parse.json) {
    request =>
      /*
       * request.body is a JsValue.
       * There is an implicit Writes that turns this JsValue as a JsObject,
       * so you can call insert() with this JsValue.
       * (insert() takes a JsObject as parameter, or anything that can be
       * turned into a JsObject using a Writes.)
       */
      request.body.validate[Question].map {
        question =>

          //`question` is an instance of the case class `models.Question`
          question.created = Calendar.getInstance().getTime
          question.qid = UUID.randomUUID().toString
          question.user = request.identity.loginInfo
          question.userName = request.identity.fullName.get
          question.rating = 0
          question.badRating = 0
          question.rel = STARTDATE.until(LocalDate.now(), ChronoUnit.DAYS)
          actionService.saveAction(new UserAction(question.user,
            ActionType.CreateMessage, Option(question.qid), None, question.created))

          collection.insert(question).map {
            lastError =>
              logger2.info(s"Successfully inserted question: $question with LastError: $lastError")
              Created(question.qid)
          }
      }.getOrElse(Future.successful(BadRequest("invalid json")))
  }

  def searchQuestions(lat1: Double, lng1: Double, lat2: Double, lng2: Double, onlyMine: Boolean, phrase: String) = UserAwareAction.async { implicit request =>
    logger2.info(s"searchQuestions requested with params: lat1 = $lat1 , lng1 = $lng1, lat2 = $lat2, lng2 = $lng2, onlyMine = $onlyMine, phrase = $phrase ")

    val user: LoginInfo = request.identity match {
      case Some(identity) => {
        identity.loginInfo
      }
      case None => NULLUSER
    }
    /*    val queryMap : List[(String, JsValueWrapper)]   = List(
          "loc" -> Json.obj("$within" -> Json.obj("$box" -> Json.arr(Json.arr(lng1, lat1), Json.arr(lng2, lat2))))
        )

        val user : LoginInfo = request.identity match {
          case Some(identity) => {
            queryMap :+ ("user" -> Json.toJson(identity.loginInfo))
            identity.loginInfo
          }
          case None => NULLUSER
        }
    */
    var query: JsObject = Json.obj()
    if (onlyMine) {
      query = Json.obj("loc" -> Json.obj("$within" -> Json.obj("$box" -> Json.arr(Json.arr(lng1, lat1), Json.arr(lng2, lat2)))),
        "$text" -> Json.obj("$search" -> phrase),
        "user" -> user)
    } else {
      query = Json.obj("loc" -> Json.obj("$within" -> Json.obj("$box" -> Json.arr(Json.arr(lng1, lat1), Json.arr(lng2, lat2)))),
        "$text" -> Json.obj("$search" -> phrase))
    }
    val prj: JsObject = Json.obj("score" -> Json.obj("$meta" -> "textScore"))

    // let's do our query
    val cursor: Cursor[Question] = collection.
      // find all
      find(query, prj).
      /*
            find(Json.obj(
              "lat" -> Json.obj("$gt" -> latitude1 , "$lt" -> latitude2),
              "lng" -> Json.obj("$gt" -> longitude1 , "$lt" -> longitude2))).
      */
      // find(Json.obj("lat" -> 12)).
      // genericQueryBuilder.
      // sort them by creation date
      sort(Json.obj("score" -> Json.obj("$meta" -> "textScore"), "rel" -> -1)).
      // perform the query and get a cursor of JsObject
      cursor[Question]

    // gather all the JsObjects in a list
    val futureQuestionsList: Future[List[Question]] = cursor.collect[List](100, false)

    // transform the list into a JsArray
    val futureQuestionsJsonArray: Future[JsArray] = futureQuestionsList.map { questions =>
      nullifyUsers(questions, user)
      Json.arr(questions)
    }

    // everything's ok! Let's reply with the array
    futureQuestionsJsonArray.map {
      questions =>
        Ok(questions(0))
    }
  }


  def findQuestions(lat1: Double, lng1: Double, lat2: Double, lng2: Double, onlyMine: Boolean, currentQuestion: String) = UserAwareAction.async { implicit request =>
    logger2.info(s"findQuestions requested with params: lat1 = $lat1 , lng1 = $lng1, lat2 = $lat2, lng2 = $lng2, onlyMine = $onlyMine ")
    val user: LoginInfo = request.identity match {
      case Some(identity) => identity.loginInfo
      case None => NULLUSER
    }
    var query1: JsObject = Json.obj()
    if (onlyMine) {
      query1 = Json.obj(
        "loc" -> Json.obj("$within" -> Json.obj("$box" -> Json.arr(Json.arr(lng1, lat1), Json.arr(lng2, lat2)))),
        "answers.0" -> Json.obj("$exists" -> true),
        "user" -> user)
    } else {
      query1 = Json.obj(
        "loc" -> Json.obj("$within" -> Json.obj("$box" -> Json.arr(Json.arr(lng1, lat1), Json.arr(lng2, lat2)))),
        "answers.0" -> Json.obj("$exists" -> true))
    }

    // let's do our query
    val cursor1: Cursor[Question] = collection.
      // find all
      find(query1).
      /*
      find(Json.obj(
        "lat" -> Json.obj("$gt" -> latitude1 , "$lt" -> latitude2),
        "lng" -> Json.obj("$gt" -> longitude1 , "$lt" -> longitude2))).
*/
      // find(Json.obj("lat" -> 12)).
      // genericQueryBuilder.
      // sort them by creation date
      sort(Json.obj("rel" -> -1)).
      // perform the query and get a cursor of JsObject
      cursor[Question]

    // gather all the JsObjects in a list
    val futureQuestionsList1: Future[List[Question]] = cursor1.collect[List](50, false)

    var query2: JsObject = Json.obj()
    if (onlyMine) {
      query2 = Json.obj(
        "loc" -> Json.obj("$within" -> Json.obj("$box" -> Json.arr(Json.arr(lng1, lat1), Json.arr(lng2, lat2)))),
        "answers.0" -> Json.obj("$exists" -> false),
        "user" -> user)
    } else {
      query2 = Json.obj(
        "loc" -> Json.obj("$within" -> Json.obj("$box" -> Json.arr(Json.arr(lng1, lat1), Json.arr(lng2, lat2)))),
        "answers.0" -> Json.obj("$exists" -> false))
    }

    val cursor2: Cursor[Question] = collection.
      find(query2).
      sort(Json.obj("created" -> -1)).
      cursor[Question]

    val futureQuestionsList2: Future[List[Question]] = cursor2.collect[List](50, false)

    var allFutures: List[Future[List[Question]]] = List(futureQuestionsList1, futureQuestionsList2)
    if (currentQuestion != null && currentQuestion.nonEmpty) {
      val currentQuestionList: Future[List[Question]] = findQuestionInt(currentQuestion)
      allFutures = allFutures :+ currentQuestionList
    }
    val oneFuture: Future[List[Question]] = Future.fold(allFutures)(List[Question]())((acc, e) => {
      acc ++ e
    })

    // transform the list into a JsArray
    val futureQuestionsJsonArray: Future[JsArray] = oneFuture.map { questions =>
      val questions2: List[Question] = distinct(questions)
      nullifyUsers(questions2, user)
      Json.arr(questions2)
    }

    // everything's ok! Let's reply with the array
    futureQuestionsJsonArray.map {
      questions =>
        Ok(questions(0))
    }
  }

  private def distinct(questions: List[Question]): List[Question] = {
    val map: Map[String, Question] = Map(questions map { q => (q.qid, q) }: _*)
    map.values.toList
  }

  val NULLUSER: LoginInfo = LoginInfo("x", "723")
  val OWNERUSER: LoginInfo = LoginInfo("owner", "owner")

  private def nullifyUsers(questions: List[Question], user: LoginInfo): Unit = {
    questions.foreach { question =>
      if (question.user == user) {
        question.user = OWNERUSER
      } else {
        question.user = NULLUSER
      }
      question.answers.foreach { answer =>
        if (answer.user == user) {
          answer.user = OWNERUSER
        } else {
          answer.user = NULLUSER
        }
      }
    }
  }

  def findQuestionInt(qid: String): Future[List[Question]] = {
    logger2.info(s"findQuestionInt requested with param: id = $qid")
    // let's do our query
    val cursor: Cursor[Question] = collection.
      // find all
      find(Json.obj(
      "qid" -> qid)).
      /*
            find(Json.obj(
              "lat" -> Json.obj("$gt" -> latitude1 , "$lt" -> latitude2),
              "lng" -> Json.obj("$gt" -> longitude1 , "$lt" -> longitude2))).
      */
      // find(Json.obj("lat" -> 12)).
      // genericQueryBuilder.
      // sort them by creation date
      sort(Json.obj("rel" -> -1)).
      // perform the query and get a cursor of JsObject
      cursor[Question]

    // gather all the JsObjects in a list
    val futureQuestionsList: Future[List[Question]] = cursor.collect[List](1, false)
    futureQuestionsList
  }

  def findQuestion(qid: String) = UserAwareAction.async { implicit request =>
    val user: LoginInfo = request.identity match {
      case Some(identity) => identity.loginInfo
      case None => NULLUSER
    }

    val futureQuestionsList: Future[List[Question]] = findQuestionInt(qid)

    // transform the list into a JsArray
    val futureQuestionsJsonArray: Future[JsValue] = futureQuestionsList.map { questions =>
      if (questions.isEmpty) {
        JsNull
      } else {
        nullifyUsers(questions, user)
        Json.toJson(questions.head)
      }
    }
    // everything's ok! Let's reply with the array
    futureQuestionsJsonArray.map {
      questions =>
        Ok(questions)
    }

  }


  def recentQuestions() = SecuredAction.async(parse.json) { implicit request =>
    logger2.info(s"recentQuestions requested ")

    val user: LoginInfo = request.identity.loginInfo

    val recentFutureList: Future[List[UserAction]] = actionService.getLastActions(user, 100)

    val recentFutureIDList: Future[List[String]] = recentFutureList.map { actions =>
      actions collect new PartialFunction[UserAction, String] {
        def apply(a: UserAction) = {
          a.qid.get
        }
        override def isDefinedAt(x: UserAction): Boolean = true
      }
    }

    val recentFutureQuestionsList: Future[List[Question]] = recentFutureIDList.flatMap { ids =>
      val query: JsObject = Json.obj("qid" -> Json.obj("$in" -> Json.arr(ids)))
      val cursor: Cursor[Question] = collection.
        find(query).
        cursor[Question]
      val futureQuestions: Future[List[Question]] = cursor.collect[List](100, false)
      //try to sort according to ids order
      val idsMap : scala.collection.mutable.Map[String , Int] = scala.collection.mutable.Map[String , Int]()
      var count:Int = 0
      ids.foreach{ id: String=>
        idsMap += (id -> count)
        count +=1
      }
      val questionsMap : Future[scala.collection.mutable.Map[Int, Question]] = futureQuestions.map {questions =>
        val result :scala.collection.mutable.Map[Int, Question] =scala.collection.mutable.Map[Int, Question]()
        questions.foreach { question =>
          result += (idsMap.get(question.qid).get -> question)
        }
        result
      }
      val questionsList:Future[List[Question]] = questionsMap.map {qMap : scala.collection.mutable.Map[Int, Question] =>
        val result = scala.collection.mutable.MutableList[Question]()
        for(i <- 0 to 100){
          result += qMap.get(i).get
        }
        List(result.toList: _*)
      }

      questionsList
    }

    // transform the list into a JsArray
    val futureQuestionsJsonArray: Future[JsArray] = recentFutureQuestionsList.map { questionsList =>
      nullifyUsers(questionsList, user)
      Json.arr(questionsList)
    }

    // everything's ok! Let's reply with the array
    futureQuestionsJsonArray.map {
      questions =>
        Ok(questions(0))
    }
  }

  def deleteQuestion(qid: String) = SecuredAction {
    request =>
      val userInfo = request.identity.loginInfo
      val qidSelector = Json.obj("qid" -> qid, "user" -> userInfo)
      val result: Future[Result] = collection.remove(qidSelector).map {
        lastError: WriteResult =>
          logger2.info(s"Successfully delete question = $qid with LastError: $lastError")
          if (lastError.n > 0) {
            Created(s"Question deleted")
          } else {
            NotAcceptable(s"The question was not deleted")
          }
      }
      result onFailure {
        case t => logger2.error(s"delete Question error: $t")
      }
      Await.result(result, 10.second)
  }

  def deleteAnswer(qid: String, aid: String) = SecuredAction {
    request =>
      val userInfo = request.identity.loginInfo
      val qidSelector = Json.obj("qid" -> qid)
      val update = Json.obj("$pull" -> Json.obj("answers" -> Json.obj("aid" -> aid, "user" -> userInfo)))
      val result: Future[Result] = collection.update(qidSelector, update).map {
        lastError: UpdateWriteResult =>
          logger2.info(s"Successfully delete answer $aid in question $qid with LastError: $lastError")
          if (lastError.nModified > 0) {
            Created(s"Answer deleted")
          } else {
            NotAcceptable(s"The answer was not deleted")
          }
      }
      result onFailure {
        case t => logger2.error(s"delete Answer error: $t")
      }
      Await.result(result, 10.second)
  }

  def createAnswer = SecuredAction(parse.json) {
    request =>
      request.body.validate[Answer].map {
        answer =>
          answer.aid = UUID.randomUUID().toString
          answer.created = Calendar.getInstance().getTime
          answer.user = request.identity.loginInfo
          answer.userName = request.identity.fullName.get
          actionService.saveAction(new UserAction(answer.user,
            ActionType.Reply, Option(answer.qid), None, answer.created))

          //`answer` is an instance of the case class `models.Answer`
          val qidSelector = Json.obj("qid" -> answer.qid)
          val update = Json.obj("$push" -> Json.obj("answers" -> Json.toJson(answer)))
          val result: Future[Result] = collection.update(qidSelector, update).map {
            lastError =>
              logger2.info(s"Successfully updated question with answer $answer with LastError: $lastError")
              Created(s"Answer Created")
          }
          result onFailure {
            case t => logger2.error(s"create Answer $answer with error: $t")
          }
          Await.result(result, 10.second)
      }.getOrElse(BadRequest("invalid json"))
  }
}
