package filters

import org.slf4j.{Logger, LoggerFactory}
import play.api.Play
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

/**
  * Created by akirichkov on 06.07.16.
  */
object HTTPSRedirectFilter extends Filter {
  private final val loggerMy: Logger = LoggerFactory.getLogger(HTTPSRedirectFilter.getClass)

  def apply(nextFilter: RequestHeader => Future[Result])
           (requestHeader: RequestHeader): Future[Result] = {
    val isProd: Boolean = Play.current.configuration.getBoolean("prod.mode").getOrElse(false) //&& !requestHeader.host.startsWith("192.168.0")
    val isSecure = requestHeader.secure || requestHeader.headers.get("X-Forwarded-Proto").exists(_ == "https")
    loggerMy.info(s"The request in prod.mode=$isProd and under https=$isSecure")
    if (!isSecure)
      if (isProd) {
        val host : String = requestHeader.headers.get("Host_nginx").head
        loggerMy.info("The redirect was triggered " + "https://" + host + requestHeader.uri)
        Future.successful(Results.MovedPermanently("https://" + host + requestHeader.uri))
      } else {
        nextFilter(requestHeader)
      }
    else
      nextFilter(requestHeader).map(_.withHeaders("Strict-Transport-Security" -> "max-age=31536000; includeSubDomains"))
  }
}
