class UserFactory

  @headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
  @defaultConfig = { headers: @headers }

#  @_dataObj = {}

  dataObj : {}

  constructor: (@$log, @$http, @$q) ->
    @$log.debug "constructing User Factory"

  get: () ->
    @$log.debug "get user started"
    deferred = @$q.defer()

    @$http.get('/user')
    .success((data, status, headers) =>
      @$log.info("Successfully get User - status #{status}")
      this.dataObj.string = data.fullName
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed get User - status #{status}")
      deferred.reject(data)
    )
    deferred.promise


servicesModule.service('UserFactory', ['$log', '$http', '$q', UserFactory])