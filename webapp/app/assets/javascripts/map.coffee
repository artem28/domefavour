
app.controller('MapCoordinatesCtrl', ($scope, $modal, $compile, $log, ipCookie, $timeout, QuestionService, MapData) ->
  TILE_SIZE = 456
  $scope.QuestionService = QuestionService
  $scope.positions = [{}]
  $scope.map
  $scope.mapData = MapData
  #chicago
  $scope.mapCenter = {lat:41.850033, lng:-87.6500523}
  $scope.showQuestionFlag = false


  showQuestion = ()->
    elem = angular.element( document.querySelector( '#showquestionElement' ) );
    if(elem.length)
      $scope.mapData.currentQuestion = elem[0].attributes['qid'].value
      $scope.mapCenter.lat = elem[0].attributes['lat'].value
      $scope.mapCenter.lng = elem[0].attributes['lon'].value
      $scope.showQuestionFlag = true


  loadMarkers =(bounds) ->
    lat1 = bounds.getNorthEast().lat()
    lng1 = bounds.getNorthEast().lng()
    lat2 = bounds.getSouthWest().lat()
    lng2 = bounds.getSouthWest().lng()
    $scope.mapData.lat1 = lat1
    $scope.mapData.lng1 = lng1
    $scope.mapData.lat2 = lat2
    $scope.mapData.lng2 = lng2
    currentQuestion = $scope.mapData.currentQuestion
    $log.debug "Bounds: #{bounds} "
    $log.debug "Bounds: NorthEast lat:#{bounds.getNorthEast().lat()} "
    $log.debug "Bounds: NorthEast lng:#{bounds.getNorthEast().lng()} "
    $log.debug "Bounds: SouthWest lat:#{bounds.getSouthWest().lat()} "
    $log.debug "Bounds: SouthWest lng:#{bounds.getSouthWest().lng()} "
    $scope.QuestionService.listQuestions(lat1, lng1, lat2, lng2, $scope.mapData.onlyMine, currentQuestion)
    .then(
      (data) =>
        $log.debug "Promise returned questions: #{data.length} Questions"
        $scope.positions = []
        if data.length > 0
          for j in [0..data.length-1]
            $log.debug "Question: #{data[j].qid} lat:#{data[j].lat} lng:#{data[j].lng}"
            icon = "green"
            if  (data[j].answers.length > 0)
              icon = "blue"
            if (currentQuestion == data[j].qid)
              icon = "orange"
            $scope.positions.push({qid:data[j].qid, lat:data[j].lat, lng: data[j].lng, icon: icon})
    ,
      (error) =>
        $log.error "Unable to get Questions: #{error}"
    )

  getCenter = () ->
    lng = parseInt(ipCookie('favoriteLng'))
    lat = parseInt(ipCookie('favoriteLat'))
    center = new google.maps.LatLng $scope.mapCenter.lat, $scope.mapCenter.lng
    if lng && lat && !$scope.showQuestionFlag
      center = new google.maps.LatLng lat, lng
    center

  putCenter = (lngLat) ->
    ipCookie('favoriteLng', lngLat.lng(), { expires: 999 })
    ipCookie('favoriteLat', lngLat.lat(), { expires: 999 })

  getZoom = () ->
    zoom = parseInt (ipCookie('favoriteZoom'))
    if !zoom
      zoom = 8
    zoom

  putZoom = (zoom) ->
    ipCookie('favoriteZoom', zoom, { expires: 999 })

  bound = (value, opt_min, opt_max) ->
    value = Math.max(value, opt_min) if opt_min
    value = Math.min(value, opt_max) if opt_max
    value

  degreesToRadians = (deg) -> deg * (Math.PI / 180)


  radiansToDegrees = (rad) -> rad / (Math.PI / 180)

  checkZoom = (map) ->
    if (map.getZoom() < 3)
      map.setZoom(3)


  checkBounds = (map) ->
    northEast = map.getBounds().getNorthEast();
    southWest = map.getBounds().getSouthWest();
    C = map.getCenter();
    X = C.lng();
    Y = C.lat();

    h2 = (northEast.lat() - southWest.lat())/2

    if (northEast.lng() > southWest.lng())
      w2 = (northEast.lng() - southWest.lng())/2
    else
      w2 = ((180-southWest.lng())+(180+northEast.lng()))/2

    maxWest = -179 + w2
    maxEast = 179 - w2
    maxNorth = 89 - h2
    maxSouth = -89 + h2

    if (X < maxWest)
      X = maxWest
    if (X > maxEast)
      X = maxEast
    if (Y < maxSouth)
      Y = maxSouth
    if (Y > maxNorth)
      Y = maxNorth

    map.setCenter(new google.maps.LatLng(Y,X));

  removeMarker = (markerId)->
    iidd = -1
    count = 0
    $scope.positions.forEach((pos) ->
      if(pos.qid == markerId)
        iidd = count
      count++
    )
    if(iidd > -1)
      $scope.positions.splice(iidd, 1);

  updateMarker = (markerId, qid)->
    $scope.positions.forEach((pos) ->
      if(pos.qid == markerId)
        pos.qid = qid
        return
    )


  MercatorProjection = () ->
    this.pixelOrigin_ = new google.maps.Point  TILE_SIZE / 2, TILE_SIZE / 2
    this.pixelsPerLonDegree_ = TILE_SIZE / 360
    this.pixelsPerLonRadian_ = TILE_SIZE / (2 * Math.PI)
    return

  MercatorProjection::fromLatLngToPoint = (latLng,  opt_point) ->
    me = this
    point = opt_point || new google.maps.Point 0, 0
    origin = me.pixelOrigin_
    point.x = origin.x + latLng.lng() * me.pixelsPerLonDegree_
    # Truncating to 0.9999 effectively limits latitude to 89.189. This is
    # about a third of a tile past the edge of the world tile.
    siny = bound Math.sin(degreesToRadians latLng.lat()), -0.9999,  0.9999
    point.y = origin.y + 0.5 * Math.log((1 + siny) / (1 - siny)) * -me.pixelsPerLonRadian_
    point


  MercatorProjection::fromPointToLatLng = (point) ->
    me = this
    origin = me.pixelOrigin_
    lng = (point.x - origin.x) / me.pixelsPerLonDegree_
    latRadians = (point.y - origin.y) / -me.pixelsPerLonRadian_
    lat = radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2)
    new google.maps.LatLng lat, lng

  $scope.$on "myEvent", (event, args) ->
    $log.debug "myEvent: #{event} args: #{args}"
    loadMarkers $scope.map.getBounds()


  $scope.$on 'mapInitialized', (event, map) ->

    showQuestion()

    $scope.map = map
    map.setZoom getZoom()
    map.setCenter getCenter()
    numTiles = 1 << map.getZoom();
    projection = new MercatorProjection()
    $scope.center = map.getCenter()
    $scope.worldCoordinate = projection.fromLatLngToPoint $scope.center
    $scope.pixelCoordinate = new google.maps.Point  $scope.worldCoordinate.x * numTiles, $scope.worldCoordinate.y * numTiles
    $scope.tileCoordinate = new google.maps.Point Math.floor($scope.pixelCoordinate.x / TILE_SIZE), Math.floor($scope.pixelCoordinate.y / TILE_SIZE)
    loadMarkers map.getBounds()

    $scope.boundsChanged = (e) ->
      origin = $scope.boundsChanged
      $scope.boundsChanged = $scope.boundsChanged2
      checkZoom(map)
      checkBounds(map)
      $timeout(()->
        $scope.boundsChanged = origin
        loadMarkers map.getBounds()
        putZoom map.getZoom()
        putCenter map.getCenter()
      ,1000)

    $scope.boundsChanged2 = (e) ->
      checkZoom(map)
      checkBounds(map)

    $scope.openQuestion = (marker, qid) ->
      modalInstance = $modal.open({
        animation: false,
        templateUrl: 'myModalQuestionViewContent.html',
        controller: 'ModalQuestionViewInstanceCtrl',
        size: 'important',
        resolve: {
          markerId: () ->
            qid
        }
      })
#      remove marker after remove
      modalInstance.result.then( () ->
        return
      ,(mid) ->
        if mid
          removeMarker(mid)
      )
    
    if($scope.showQuestionFlag)
      $scope.openQuestion(null, $scope.mapData.currentQuestion)

    $scope.placeMarker = (e) ->
      ll = e.latLng
      d = new Date();
      idd = d.getHours()*60*60*1000 + d.getMinutes()*60*1000 + d.getSeconds()*1000 + d.getMilliseconds()
      $scope.positions.push({qid:idd, lat:ll.lat(), lng: ll.lng(), icon: 'green'});
      modalInstance = $modal.open({
        animation: false,
        templateUrl: 'myModalContent.html',
        controller: 'ModalCreateQuestionInstanceCtrl',
        size: '',
        resolve: {
          markerId: () ->
            idd
          , lat: () ->
            ll.lat()
          , lng: () ->
            ll.lng()
        }
      })
# remove marker if it was wrong
      modalInstance.result.then( () ->
        updateMarker(idd, modalInstance.qid)
        return
      ,(markerId) ->
        markerId = idd
        removeMarker(idd)
      )

  return
)
