
dependencies = [
    'ngMap',
    'ngRoute',
    'ui.bootstrap',
    'myApp.filters',
    'myApp.services',
    'myApp.controllers',
    'myApp.directives',
    'myApp.common',
    'myApp.routeConfig',
    'ipCookie',
#    'angularFileUpload',
    'flow',
    'jkuri.gallery',
    'satellizer',
    'pageslide-directive'
]

@app = angular.module('myApp', dependencies)


@commonModule = angular.module('myApp.common', [])
@controllersModule = angular.module('myApp.controllers', [])
@servicesModule = angular.module('myApp.services', [])
@modelsModule = angular.module('myApp.models', [])
@directivesModule = angular.module('myApp.directives', [])
@filtersModule = angular.module('myApp.filters', [])


app.config(['flowFactoryProvider' , '$httpProvider', '$authProvider',  (flowFactoryProvider,  $httpProvider, $authProvider) ->
      flowFactoryProvider.defaults = {
        target: '/image',
        permanentErrors: [415, 500, 501],
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        simultaneousUploads: 1,
        testChunks: false
      };
      flowFactoryProvider.on('catchAll',  (event) ->
        console.log('catchAll', arguments);
      )
#      flowFactoryProvider.factory = fustyFlowFactory;



      $httpProvider.interceptors.push(($q, $injector, $window) ->
        return {
          request: (request) ->
            $auth = $injector.get('$auth');
            if ($auth.isAuthenticated())
              request.headers['X-Auth-Token'] = $auth.getToken();
            return request;
          ,responseError: (rejection) ->
            if (rejection.status == 401)
              console.log(rejection.statusText);
#              $log.info(rejection.statusText)
#              $window.alert rejection.statusText
#              $injector.get('$state').go('signIn');
            return $q.reject(rejection);
        }
      )

      $authProvider.httpInterceptor = true; #// Add Authorization header to HTTP request
      $authProvider.loginOnSignup = true;
      $authProvider.loginRedirect = '/home';
      $authProvider.logoutRedirect = '/';
      $authProvider.signupRedirect = '/home';
      $authProvider.loginUrl = '/signIn';
      $authProvider.signupUrl = '/signUp';
      $authProvider.loginRoute = '/signIn';
      $authProvider.signupRoute = '/signUp';
      $authProvider.tokenName = 'token';
      $authProvider.tokenPrefix = 'satellizer'; #// Local Storage name prefix
      $authProvider.authHeader = 'X-Auth-Token';

      $authProvider.facebook({
        clientId: '1664695150448228',
        url: '/authenticate/facebook',
        authorizationEndpoint: 'https://www.facebook.com/dialog/oauth',
        redirectUri: window.location.origin || window.location.protocol + '//' + window.location.host + '/',
        scope: 'email',
        scopeDelimiter: ',',
        requiredUrlParams: ['display', 'scope'],
        display: 'popup',
        type: '2.0',
        popupOptions: { width: 481, height: 269 }
      })

      $authProvider.google({
        clientId: '314129221533-of8t8g12b5pj04h1v5fd95t0mocebf21.apps.googleusercontent.com',
        url: '/authenticate/google',
        authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
        redirectUri: window.location.origin || window.location.protocol + '//' + window.location.host,
        scope: ['profile', 'email'],
        scopePrefix: 'openid',
        scopeDelimiter: ' ',
        requiredUrlParams: ['scope'],
        optionalUrlParams: ['display'],
        display: 'popup',
        type: '2.0',
        popupOptions: { width: 580, height: 550 }
      });
  
      $authProvider.twitter({
        url: '/authenticate/twitter',
        type: '1.0',
        popupOptions: { width: 495, height: 645 }
      });
])


angular.module('myApp.routeConfig', ['ngRoute'])
    .config(['$routeProvider', ($routeProvider) ->
        $routeProvider
            .when('/', {
                templateUrl: '/assets/partials/view.html'
            })
            .when('/users/create', {
                templateUrl: '/assets/partials/create.html'
            })
            .when('/users/edit/:firstName/:lastName', {
                templateUrl: '/assets/partials/update.html'
            })
            .otherwise({redirectTo: '/'})
    ])
    .config(['$locationProvider', ($locationProvider) ->
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
    })
  ])


app.run(($rootScope) ->
  $rootScope.user = {}
)

app.factory('MapData',  () ->
  return { lat1: 0, lng1: 0, lat2: 0, lng2: 0, onlyMine: false, currentQuestion: ""};
)

app.controller('MainAuthCtrl',['$scope', '$log', '$auth', '$window', '$q', '$http', 'UserFactory', "$modal", "MapData", ($scope, $log, $auth, $window, $q, $http, UserFactory, $modal, MapData) ->
  $scope.user = UserFactory.dataObj
  $scope.searchPhrase = ""
  $scope.mapData = MapData
  $scope.showMine = $scope.mapData.onlyMine

  $scope.authenticate = (provider) ->
    $auth.authenticate(provider)
    .then(() ->
#      $window.alert('You have successfully signed in')
      getUser()
      $log.info "You have successfully signed in as #{$scope.user.string}"
    )
    .catch((response) ->
      $log.error "authenticate catch error response: #{response}"
      $log.error "authenticate catch error response: #{response.data}"
      $log.error "authenticate catch error response: #{response.data.message}"
#      $window.alert(response.data.message)
    )

  $scope.showOnlyMine = (showMine) ->
    $log.info "showMine = #{showMine}; $scope.mapData.onlyMine =  #{$scope.mapData.onlyMine}"
    $scope.mapData.onlyMine = showMine
    $scope.$root.$broadcast("myEvent", {});


  $scope.isAuthenticated = () ->
    $auth.isAuthenticated();

  $scope.signOut = () ->
    $scope.mapData = false
    if (!$auth.isAuthenticated())
      return;

    deferred = $q.defer()
    $http.get('/signout')
    .success((data, status, headers) ->
      $log.info("Successfully signout - status #{status}")

      $auth.logout()
      .then(() ->
        $log.info ("You have been logged out")
      )

      deferred.resolve(data)
    )
    .error((data, status, headers) ->
      $log.error("Failed to signout - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  getUser = () ->
    UserFactory.get()
      .then(
        (data) =>
          $log.info("getUser - success #{data}")
#          $rootScope.user = data;
#          $scope.user = data;
        ,
        (error) =>
          $log.error("get user error: #{error.message}")
#          $window.alert(error.message)
      )

  $scope.openSearch = () ->
    if (!!$scope.searchPhrase)
      $modal.open({
        animation: false,
        templateUrl: 'myModalSearchResultContent.html',
        controller: 'ModalSearchResultInstanceCtrl',
        size: 'important',
        resolve: {
          searchPhrase: () ->
            $scope.searchPhrase
        }
      })

  $scope.init = () ->
    getUser()

])