app.controller('ModalCreateQuestionCtrl', ($scope, $modal, $log) ->
)

app.controller('ModalCreateQuestionInstanceCtrl',['$scope', '$modalInstance', '$modal', '$log', '$auth', 'QuestionService', 'markerId', 'lng', 'lat', 'MapData', ($scope, $modalInstance, $modal, $log, $auth, QuestionService,  markerId, lng, lat, MapData) ->
  $scope.QuestionService= QuestionService
  $scope.markerId = markerId
  $scope.lng = lng
  $scope.lat = lat
  $scope.mapData = MapData
  $scope.question = {text : 'Type here your message', loc:{lon: lng, lat: lat}, lng: lng, lat: lat, created: new Date(), qid: "000", rel:0, rating: 0, badRating: 0, files:[],  answers:[], userName:"", user:{providerID: "x", providerKey:"723"}}
  $scope.blockAdd = false

  #  $scope.ok = () ->
  #    $modalInstance.close ""
  $scope.fileMessageUploadSuccess = (file, flow, msg) ->
    msgJson = JSON.parse(msg)
    file.fileId = msgJson.fileId
    file.thumbId = msgJson.thumbId
    file.contentType = msgJson.contentType
    $log.info "message file upload finished: #{file.fileId}"

  $scope.errors = [];

  $scope.validate =  (file) ->
    if (file.size > 1024000)
      $scope.errors.push({file:file, error: "file is too big"});
      return false;
    if (!{png:1,gif:1,jpg:1,jpeg:1}[file.getExtension()])
      $scope.errors.push({file:file, error: "file has wrong file type"});
      return false;
    return true;

  $scope.createQuestion = (flow) ->
    $log.debug "createQuestion was started"
    files = []
    if('Type here your message' == $scope.question.text)
      $scope.question.text = ""

    flow.files.forEach((file)->
      $log.info "file in message: #{file.fileId}"
      text_field = ""
      if (file.text?)
        text_field = file.text
      files.push {fileId:file.fileId, text:text_field,  contentType:file.contentType, thumbId:file.thumbId, name:file.name }
    )

    $scope.question.files = files

    $scope.QuestionService.createQuestion($scope.question)
    .then(
      (data) =>
        $modalInstance.qid = data
        $scope.mapData.currentQuestion = data
        $scope.$root.$broadcast("myEvent", {});
        $modalInstance.close()
    ,
      (error) =>
        $log.error "Unable to create Question: #{error}"
        $scope.cancel()
    )

  $scope.openLoginWindow = () ->
    $modal.open({
      animation: false,
      templateUrl: 'myModalLoginContent.html',
      controller: 'ModalLoginInstanceCtrl',
      size: 'sm',
      resolve: {}
    })

  $scope.isAuthenticated = () ->
    $auth.isAuthenticated();

  $scope.messageFileCancel = (file) ->
    file.cancel()


  $scope.cancel = () ->
    $modalInstance.dismiss $scope.markerId

  return
])