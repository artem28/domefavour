app.controller('ModalSearchResultCtrl', ($scope, $modal, $log) ->
)

app.controller('ModalSearchResultInstanceCtrl', ['$scope', '$modalInstance', '$modal', '$log', 'QuestionService', 'searchPhrase', 'MapData',($scope, $modalInstance, $modal, $log, QuestionService,  searchPhrase, MapData) ->
  $scope.QuestionService= QuestionService
  $scope.searchPhrase = searchPhrase
  $scope.mapData = MapData
  $scope.questions = [{}]
  $scope.bigCurrentPage = 1
  $scope.bigTotalItems = 0
  $scope.numPerPage = 10


  search = ()->
    loadMarkers($scope.mapData.lat1, $scope.mapData.lng1, $scope.mapData.lat2 , $scope.mapData.lng2 )


  loadMarkers =(lat1, lng1, lat2 , lng2 ) ->
    $scope.QuestionService.searchQuestions(lat1, lng1, lat2, lng2, $scope.mapData.onlyMine, $scope.searchPhrase)
    .then(
      (data) =>
        $log.debug "Promise returned questions: #{data.length} Question s"
        $scope.questions = []
        if data.length > 0
          for j in [0..data.length-1]
            $log.debug "Question: #{data[j].qid} lat:#{data[j].lat} lng:#{data[j].lng}"
            icon = "green"
            if  (data[j].answers.length > 0)
              icon = "blue"
            $scope.questions.push({question:data[j], icon: icon})
          $scope.bigTotalItems = $scope.questions.length
    ,
      (error) =>
        $log.error "Unable to get Questions: #{error}"
    )

  $scope.openQuestion = (markerId) ->
    $log.debug "Question open: #{markerId}"
    $modal.open({
      animation: false,
      templateUrl: 'myModalQuestionViewContent.html',
      controller: 'ModalQuestionViewInstanceCtrl',
      size: 'important',
      resolve: {
        markerId: () ->
          markerId
      }
    })

  $scope.showOnMap = (qid) ->
    $scope.mapData.currentQuestion = qid
    $scope.$root.$broadcast("myEvent", {});
    $modalInstance.dismiss "close it!"

  $scope.cancel = () ->
    $modalInstance.dismiss $scope.markerId



  $scope.showOnlyMine = () ->
    return $scope.mapData.onlyMine

  search()
  return
])