app.controller('ModalAnswerCreateCtrl', ($scope, $modal, $log) ->
)


app.controller('ModalAnswerCreateInstanceCtrl',['$scope', '$modalInstance', '$modal', '$log', '$auth', 'QuestionService', 'markerId', ($scope, $modalInstance, $modal, $log, $auth, QuestionService, markerId) ->
  $scope.markerId = markerId
  $scope.answerText = "Type here your answer"
  $scope.blockAdd = false


  QuestionService.getQuestion(markerId)
  .then(
    (data) =>
      $scope.question  = data
  ,
    (error) =>
      $log.error "Unable to load a Question: #{error}"
      doClose()
  )

  $scope.fileUploadSuccess = (file, flow, msg) ->
    msgJson = JSON.parse(msg)
    file.fileId = msgJson.fileId
    file.thumbId = msgJson.thumbId
    file.contentType = msgJson.contentType
#    $scope.blockAdd = !allUploadsDone(flow)
    $log.info "file upload finished: #{file.fileId}"
#    $log.info "is Upload in progress: #{$scope.blockAdd}"

  allUploadsDone = (flow) ->
    result = true
    for file in flow.files
      do (file) ->
        result = result and !file.isUploading()
    return result

  $scope.fileCancel = (file) ->
    file.cancel()

  $scope.errors = [];

  $scope.validate =  (file) ->
    if (file.size > 1024000)
      $scope.errors.push({file:file, error: "file is too big"});
      return false;
    if (!{png:1,gif:1,jpg:1,jpeg:1}[file.getExtension()])
      $scope.errors.push({file:file, error: "file has wrong file type"});
      return false;
    return true;


  $scope.createAnswer = (flow) ->
    $log.debug "createAnswer was started"
    files = []
    if('Type here your answer' == $scope.answerText)
      $scope.answerText = ""
    answer = {qid: $scope.markerId, aid: "000", created: new Date(), text:$scope.answerText, files:files, rating: 0, badRating: 0, userName:"", user:{providerID: "x", providerKey:"723"}}

    flow.files.forEach((file)->
      $log.info "file in answer: #{file.fileId}"
      text_field = ""
      if (file.text?)
        text_field = file.text
      files.push {fileId:file.fileId, text:text_field,  contentType:file.contentType, thumbId:file.thumbId, name:file.name }
    )
    QuestionService.createAnswer(answer)
    .then(
      (data) =>
    ,
      (error) =>
        $log.error "Unable to load a Question: #{error}"
    )
    doClose()

  $scope.isAuthenticated = () ->
    $auth.isAuthenticated();

  doClose = () ->
    $modalInstance.dismiss $scope.markerId
    $modal.open({
      animation: false,
      templateUrl: 'myModalQuestionViewContent.html',
      controller: 'ModalQuestionViewInstanceCtrl',
      size: 'important',
      resolve: {
        markerId: () ->
          markerId
      }
    })

  $scope.close = () ->
    doClose()
])
