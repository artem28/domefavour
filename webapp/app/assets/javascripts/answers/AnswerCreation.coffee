
class ImageHolder

    images = []

    constructor: (@$log) ->
      @$log.debug "Image Holder was initiated"
      images = []

    reset: () ->
      images = []


    addImage: (id, fileName) ->
      @$log.debug "add image to holder #{id}, #{fileName}"
      images.push({id: id, fileName: fileName})



    showAllImages: () ->
      @$log.debug "show all images  #{images}"
      images


servicesModule.service('ImageHolder', ['$log', ImageHolder])