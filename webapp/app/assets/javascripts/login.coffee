app.controller('ModalLoginCtrl', ($scope, $modal, $log) ->
)

app.controller('ModalLoginInstanceCtrl',['$scope', '$rootScope','$modalInstance', '$modal', '$log', '$auth', '$window', '$q', '$http','UserFactory', ($scope, $rootScope, $modalInstance, $modal, $log, $auth, $window, $q, $http, UserFactory) ->
  $scope.authenticate = (provider) ->
    $auth.authenticate(provider)
    .then(() ->
#      $window.alert("Success!")
      $modalInstance.dismiss ('cancel')
      getUser()
      $rootScope.$broadcast('handleBroadcastReloadQuestion');
    )
    .catch((response) ->
      $log.error "authenticate catch error response: #{response}"
      $log.error "authenticate catch error response: #{response.data}"
      $log.error "authenticate catch error response: #{response.data.message}"
#      $window.alert(response.data.message)
      $modalInstance.dismiss ('cancel')
    )

  getUser = () ->
    UserFactory.get()
    .then(
      (data) =>
        $log.info("getUser - success #{data}")
    ,
      (error) =>
        $log.error("get user error: #{error.message}")
#       $window.alert(error.message)
    )

  $scope.isAuthenticated = () ->
    $auth.isAuthenticated();

])