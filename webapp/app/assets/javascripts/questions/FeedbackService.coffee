
class FeedbackService

    @headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
    @defaultConfig = { headers: @headers }

    constructor: (@$log, @$http, @$q) ->
        @$log.debug "constructing Feedback Service"

    getFeedback: (qid) ->
      @$log.debug "getFeedback()"
      deferred = @$q.defer()

      @$http.get("/feedback", {params:{qid: qid}})
      .success((data, status, headers) =>
        @$log.info("Successfully search Feedback - status #{status}")
        deferred.resolve(data)
      )
      .error((data, status, headers) =>
        @$log.error("Failed to search Feedback- status #{status}")
        deferred.reject(data)
      )
      deferred.promise

    provideFeedback: (feedback, positive) ->
      @$log.debug "provideFeedback #{angular.toJson(feedback, true)}"
      deferred = @$q.defer()
      command = '/negative'
      if(positive)
        command = '/positive'

      @$http.post(command, feedback)
      .success((data, status, headers) =>
                @$log.info("Successfully created feedback - status #{status}")
                deferred.resolve(data)
            )
      .error((data, status, headers) =>
                @$log.error("Failed to create feedback - status #{status}")
                deferred.reject(data)
            )
      deferred.promise

servicesModule.service('FeedbackService', ['$log', '$http', '$q', FeedbackService])