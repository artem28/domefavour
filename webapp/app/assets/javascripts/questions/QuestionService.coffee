
class QuestionService

    @headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
    @defaultConfig = { headers: @headers }

    constructor: (@$log, @$http, @$q) ->
        @$log.debug "constructing Question Service"

    searchQuestions: (lat1, lng1, lat2, lng2, onlyMine, phrase) ->
      @$log.debug "searchQuestions()"
      deferred = @$q.defer()

      @$http.get("/search", {params:{lat1: lat1, lng1: lng1, lat2: lat2, lng2: lng2, onlyMine: onlyMine, phrase: phrase}})
      .success((data, status, headers) =>
        @$log.info("Successfully search Questions - status #{status}")
        deferred.resolve(data)
      )
      .error((data, status, headers) =>
        @$log.error("Failed to search Questions - status #{status}")
        deferred.reject(data)
      )
      deferred.promise

    listQuestions: (lat1, lng1, lat2, lng2, onlyMine, qid) ->
      @$log.debug "listQuestions()"
      deferred = @$q.defer()

      @$http.get("/questions", {params:{lat1: lat1, lng1: lng1, lat2: lat2, lng2: lng2, onlyMine: onlyMine, currentQuestion: qid}})
      .success((data, status, headers) =>
        @$log.info("Successfully listed Questions - status #{status}")
        deferred.resolve(data)
      )
      .error((data, status, headers) =>
        @$log.error("Failed to list Questions - status #{status}")
        deferred.reject(data)
      )
      deferred.promise

    recentQuestions: () ->
      @$log.debug "recentQuestions()"
      deferred = @$q.defer()

      @$http.get("/recent")
      .success((data, status, headers) =>
        @$log.info("Successfully has got recent Questions - status #{status}")
        deferred.resolve(data)
      )
      .error((data, status, headers) =>
        @$log.error("Failed to get recent Questions - status #{status}")
        deferred.reject(data)
      )
      deferred.promise

    getQuestion: (qid) ->
      @$log.debug "getQuestion()"
      deferred = @$q.defer()

      @$http.get("/question", {params:{qid: qid}})
      .success((data, status, headers) =>
        @$log.info("Successfully get Question - status #{status}")
        deferred.resolve(data)
      )
      .error((data, status, headers) =>
        @$log.error("Failed to get Question - status #{status}")
        deferred.reject(data)
      )
      deferred.promise

    removeQuestion: (qid) ->
      @$log.debug "removeQuestion()"
      deferred = @$q.defer()

      @$http.delete("/question", {params:{qid: qid}})
      .success((data, status, headers) =>
        @$log.info("Successfully delete Question - status #{status}, data #{data}")
        deferred.resolve(data)
      )
      .error((data, status, headers) =>
        @$log.error("Failed to delete Question - status #{status}")
        deferred.reject(data)
      )
      deferred.promise

    removeAnswer: (qid, aid) ->
      @$log.debug "removeAnswer()"
      deferred = @$q.defer()

      @$http.delete("/answer", {params:{qid: qid, aid: aid}})
      .success((data, status, headers) =>
        @$log.info("Successfully delete Answer - status #{status}")
        deferred.resolve(data)
      )
      .error((data, status, headers) =>
        @$log.error("Failed to delete Answer - status #{status}")
        deferred.reject(data)
      )
      deferred.promise

    createQuestion: (question) ->
        @$log.debug "createQuestion #{angular.toJson(question, true)}"
        deferred = @$q.defer()

        @$http.post('/question', question)
        .success((data, status, headers) =>
                @$log.info("Successfully created Question - status #{status}")
                deferred.resolve(data)
            )
        .error((data, status, headers) =>
                @$log.error("Failed to create question - status #{status}")
                deferred.reject(data)
            )
        deferred.promise

    createAnswer: (answer) ->
      @$log.debug "createAnswer #{angular.toJson(answer, true)}"
      deferred = @$q.defer()

      @$http.post('/answer', answer)
      .success((data, status, headers) =>
        @$log.info("Successfully created Answer - status #{status}")
        deferred.resolve(data)
      )
      .error((data, status, headers) =>
        @$log.error("Failed to create answer - status #{status}")
        deferred.reject(data)
      )
      deferred.promise


servicesModule.service('QuestionService', ['$log', '$http', '$q', QuestionService])