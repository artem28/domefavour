app.controller('ModalQuestionViewCtrl', ($scope, $modal, $log) ->
)


app.controller('ModalQuestionViewInstanceCtrl',['$scope', '$rootScope', '$modalInstance', '$modal', '$log', '$auth', '$window', 'QuestionService', 'FeedbackService', 'markerId', '$q', '$http', 'MapData', ($scope, $rootScope, $modalInstance, $modal, $log, $auth, $window, QuestionService, FeedbackService, markerId, $q, $http, MapData) ->
  $scope.markerId = markerId
  $scope.mapData = MapData

  $scope.bigCurrentPage = 1
  $scope.bigTotalItems = 0
  $scope.numPerPage = 3
  $scope.feedback = {}
  $scope.question = {}


  prepareImages = (answerOrQuestion) ->
    images=[]
    if(answerOrQuestion.files)
      answerOrQuestion.files.forEach((file)->
        images.push {thumb: "/image?qid=#{file.thumbId}", img: "/image?qid=#{file.fileId}", qid:file.fileId,  description: file.text}
      )
    return images

  getQuestion = () ->
    QuestionService.getQuestion($scope.markerId)
    .then(
      (data) =>
        if !data
          $modalInstance.dismiss "error to get question"
          return
        $scope.question  = data
        $scope.mapData.currentQuestion = data.qid
        $scope.$root.$broadcast("myEvent", {});

        $scope.question.gallery = prepareImages($scope.question)


        if ($scope.question.answers && $scope.question.answers.length > 0)
          $scope.bigTotalItems = $scope.question.answers.length
          $scope.question.answers.forEach( (answer) ->
            answer.gallery = prepareImages(answer)
          )
        if ($auth.isAuthenticated())
          getFeedback($scope.question.qid, $scope.question.loc.lon, $scope.question.loc.lat )
    ,
      (error) =>
        $log.error "Unable to load a Question: #{error}"
        $modalInstance.dismiss "error to load question"
    )
    #  {text : 'Show me what do you see there', loc:{lon: 0, lat: 0}, lng: 0, lat: 0, created: new Date(), qid: markerId}

  getFeedback = (qid, lng, lat) ->
    FeedbackService.getFeedback(qid)
    .then(
      (data) =>
        if !data
          $scope.feedback = {qid: qid, created: new Date(), loc:{lon: lng, lat: lat}, answers:{}}
        else
          $scope.feedback  = data
    ,
      (error) =>
        $log.error "Unable to load a Feedback: #{error}"
    )

  getQuestion()

  $scope.showFeedbackQuestion = () ->
    result = $auth.isAuthenticated() && ('feedback' of $scope) && ($scope.feedback != null) && ('qid' of $scope.feedback) && !('positive' of $scope.feedback)
    $log.info "showFeedbackQuestion = #{result}"
    result

  $scope.showFeedbackAnswer = (aid) ->
    result = $auth.isAuthenticated() && ('feedback' of $scope) && ($scope.feedback != null) && ('answers' of $scope.feedback) && !(aid of $scope.feedback.answers)
    $log.info "showFeedbackAnswer = #{result}"
    result

  $scope.likeQuestion = () ->
    $scope.feedback.positive = true
    $scope.question.rating++
    updateFeedback(true)

  $scope.stopQuestion = () ->
    $scope.feedback.positive = false
    $scope.question.badRating++
    updateFeedback(false)

  $scope.likeAnswer = (aid) ->
    $scope.feedback.answers[aid] = true
    ($scope.question.answers.filter (i) -> i.aid is aid)[0].rating++
    updateFeedback(true)

  $scope.stopAnswer = (aid) ->
    $scope.feedback.answers[aid] = false
    ($scope.question.answers.filter (i) -> i.aid is aid)[0].badRating++
    updateFeedback(false)

  updateFeedback = (positive) ->
    FeedbackService.provideFeedback($scope.feedback, positive)
    .then(
      (data) =>
        $log.info "The Feedback was provided: #{data}"
    ,
      (error) =>
        $log.error "Unable to provide feedback: #{error}"
    )

  $scope.removeQuestion = (qid) ->
    QuestionService.removeQuestion(qid)
      .then(
        (data) =>
          $log.info "The Question was removed: #{data}"
          $modalInstance.dismiss $scope.markerId
      ,
        (error) =>
          $log.error "Unable to remove a Question: #{error}"
          $modalInstance.dismiss "error to remove question"
      )

  $scope.removeAnswer = (qid, aid) ->
    QuestionService.removeAnswer(qid, aid)
    .then(
      (data) =>
        $log.info "The Answer was removed: #{data}"
        $scope.refreshQuestion()
#        removeAnswerFromDialog(aid)
    ,
      (error) =>
        $log.error "Unable to remove a answer: #{error}"
    )


  $scope.refreshQuestion = () ->
    $modalInstance.dismiss "refresh"
    $modal.open({
      animation: false,
      templateUrl: 'myModalQuestionViewContent.html',
      controller: 'ModalQuestionViewInstanceCtrl',
      size: 'important',
      resolve: {
        markerId: () ->
          $scope.markerId
      }
    })

  $scope.cancel = () ->
    $modalInstance.dismiss "cancel"

  $scope.closeQuestionOpenAnswer = () ->
    $modalInstance.dismiss "open answer"
    $modal.open({
      animation: false,
      templateUrl: 'myModalAnswerCreateContent.html',
      controller: 'ModalAnswerCreateInstanceCtrl',
      size: 'important',
      resolve: {
        markerId: () ->
          $scope.markerId
      }
    })

  $scope.openLoginWindow = () ->
    $modal.open({
      animation: false,
      templateUrl: 'myModalLoginContent.html',
      controller: 'ModalLoginInstanceCtrl',
      size: 'sm',
      resolve: {}
    })

#    Authentification
  $scope.authenticate = (provider) ->
    $auth.authenticate(provider)
      .then(() ->
#        $window.alert('You have successfully signed in')
        $log.info 'You have successfully signed in'
      )
      .catch((response) ->
        $log.error "authenticate catch error response: #{response}"
        $log.error "authenticate catch error response: #{response.data}"
        $log.error "authenticate catch error response: #{response.data.message}"
#        $window.alert(response.data.message)
      )


  $scope.isAuthenticated = () ->
     $auth.isAuthenticated();

  $scope.signOut = () ->
    if (!$auth.isAuthenticated())
      return;

    deferred = $q.defer()
    $http.get('/signout')
      .success((data, status, headers) ->
        $log.info("Successfully signout - status #{status}")

        $auth.logout()
          .then(() ->
            $log.info ("You have been logged out")
          )

        deferred.resolve(data)
      )
      .error((data, status, headers) ->
        $log.error("Failed to signout - status #{status}")
        deferred.reject(data)
      )
    deferred.promise

  $scope.$on('handleBroadcastReloadQuestion', () ->
    getQuestion()
  )
])