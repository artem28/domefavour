package services

import java.io.{OutputStream, IOException, File}
import java.util
import java.util.concurrent.{LinkedBlockingQueue, TimeUnit}
import javax.imageio.ImageIO
import javax.inject.Singleton
import org.imgscalr.Scalr

import com.mediafire.sdk._
import com.mediafire.sdk.response_models.data_models.{FolderModel, FolderInfoModel}
import com.mediafire.sdk.response_models.file.{FileGetLinksResponse, FileCopyResponse}
import com.mediafire.sdk.response_models.folder.{FolderGetContentsResponse, FolderGetInfoResponse}
import com.mediafire.sdk.response_models.{MediaFireApiResponse, ApiResponse}
import com.mediafire.sdk.uploader.MFFileUpload.Builder
import com.mediafire.sdk.uploader._
import org.slf4j.{LoggerFactory, Logger}
import play.api.libs.ws._
import play.api.libs.iteratee._
import play.libs.Json
import scala.collection.JavaConverters._
import scala.collection.mutable


/**
 * Created by artem on 22.09.15.
 */

@Singleton
class MFUtils  {
  private final val logger: Logger = LoggerFactory.getLogger(classOf[MFUtils])

  var client : MediaFireClient = null
  val builder : MFClient.Builder = new MFClient.Builder("47740", "th65spz9jb3zah91j18ax97jxazmulirjxp00bzs")
  var executor : PausableExecutor = new PausableExecutor(3, 3, 10L, TimeUnit.SECONDS, new LinkedBlockingQueue[Runnable])
  client = builder.build()
  client.getCredentialStore().setEmail(new MediaFireCredentialsStore.EmailCredentials("artem.kirichkov@gmail.com", "artem3081"))

  def getDirectUrl(quickKey: String): String ={
    val params: java.util.Map[String, AnyRef] = new util.HashMap[String, AnyRef]()
    params.put("quick_key", quickKey)
    params.put("link_type", "direct_download")
//    params.put("response_format", "json")
   // client.getSessionSignature()
    val request : MediaFireApiRequest = new MFApiRequest("/file/get_links.php", params, null, null)
    val response : FileGetLinksResponse = client.sessionRequest(request, classOf[FileGetLinksResponse])
    response.getLinks()(0).getDirectDownloadLink
  }

  def copyFile(quickKey: String, thumbName: String): String = {
    val params: java.util.Map[String, AnyRef] = new util.HashMap[String, AnyRef]()
    params.put("quick_key", quickKey)
    val request : MediaFireApiRequest = new MFApiRequest("file/copy.php", params, null, null);
    val response : FileCopyResponse = client.sessionRequest(request, classOf[FileCopyResponse]);
    response.getNewQuickKeys()(0)
  }


  def convertImage(thumbKey: String, origImage: File): File = {
    val file : File = File.createTempFile (thumbKey , "");
    ImageIO.write(Scalr.resize(ImageIO.read(origImage), 100), "JPG", file);
    file
  }

  def uploadFromStream(file : File) : (String ,String )={

    val folderKey: String = getRootFolderKey

    val store : UploadStoreImpl = new UploadStoreImpl()
    val uploader : MFUploader = new MFUploader(client, store, executor, 98)

    executor.resume
//    val file: File = new File("/home/artem/projects/scala/domefavor/webapp/build.sbt")

    val nanoTime = System.nanoTime
    val fileName : String = "A%d.dmf".format(nanoTime)
    val thumbName : String ="A%d.thn".format(nanoTime)

    internalUpload(file, folderKey, store, uploader, fileName)

    if(store.getQuickKey().isEmpty){
      throw new IllegalStateException("Something goes wrong with the image upload: " + fileName);
    }
    val quickKey : String = store.getQuickKey().get

    val thumbFile: File = convertImage(thumbName, file)
    store.reset()
    internalUpload(thumbFile, folderKey, store, uploader, thumbName)
    if(store.getQuickKey().isEmpty){
      throw new IllegalStateException("Something goes wrong with the thumbnail upload: " + thumbName);
    }
    val thumbKey : String = store.getQuickKey().get

    (quickKey, thumbKey)
  }

  protected def internalUpload(file: File, folderKey: String, store: UploadStoreImpl, uploader: MFUploader, fileName: String): Unit = {
    val builder: Builder = new Builder(file, fileName, folderKey)
    builder.setResumable(true)
    builder.setActionOnInAccount(MediaFireFileUpload.ActionOnInAccount.UPLOAD_ALWAYS)
    builder.setFileSize(file.length)
    builder.setSha256Hash(client.getHasher.sha256(file))
    uploader.schedule(builder.build)

    while (store.isWaitingForUploads) {
      Thread.sleep(250)
      logger.debug("uploader still running for " + fileName)
    }
  }

  protected def getRootFolderKey: String = {
    val request: MediaFireApiRequest = new MFApiRequest("/folder/get_content.php", null, null, null);
    val rootfolder: FolderGetContentsResponse = client.sessionRequest(request, classOf[FolderGetContentsResponse])

    val array: mutable.Buffer[FolderModel] = rootfolder.getFolderContents.getFolders.asScala.filter(folder => folder.getFolderName == "domefavour")
    val folderKey: String = array.head.getFolderkey
    folderKey
  }


}

class UploadStoreImpl extends MediaFireUploadStore{
  private var finished : Boolean = false
  private var quickKey = None : Option[String]

  def  isWaitingForUploads ={
    !finished
  }

  def getQuickKey() :Option[String] = {
    quickKey
  }

  def reset(): Unit ={
    finished  = false
    quickKey = None : Option[String]
  }

  override def resumableUploadStarting(mediaFireFileUpload: MediaFireFileUpload): Unit = {}

  override def instantUploadStarting(mediaFireFileUpload: MediaFireFileUpload): Unit = {}

  override def checkFinished(mediaFireFileUpload: MediaFireFileUpload): Unit = {}

  override def insert(mediaFireWebUpload: MediaFireWebUpload): Unit = {}

  override def insert(mediaFireFileUpload: MediaFireFileUpload): Unit = {}

  override def pollingReady(mediaFireWebUpload: MediaFireWebUpload, s: String): Unit = {}

  override def pollingReady(mediaFireFileUpload: MediaFireFileUpload, s: String): Unit = {}

  override def resumableFinishedWithoutAllUnitsReady(mediaFireFileUpload: MediaFireFileUpload): Unit = {}

  override def polling(mediaFireWebUpload: MediaFireWebUpload, i: Int, s: String): Unit = {}

  override def polling(mediaFireFileUpload: MediaFireFileUpload, i: Int, s: String): Unit = {}

  override def uploadQueued(mediaFireWebUpload: MediaFireWebUpload): Unit = {
    finished = false
  }

  override def uploadQueued(mediaFireFileUpload: MediaFireFileUpload): Unit = {
    finished = false
  }

  override def uploadProgress(mediaFireFileUpload: MediaFireFileUpload, v: Double): Unit = {}

  override def uploadFinished(mediaFireWebUpload: MediaFireWebUpload, s: String, s1: String): Unit = {
    quickKey = Some(s)
    finished = true
  }

  override def uploadFinished(mediaFireFileUpload: MediaFireFileUpload, s: String, s1: String): Unit ={
    quickKey = Some(s)
    finished = true
  }

  override def pollingLimitExceeded(mediaFireWebUpload: MediaFireWebUpload): Unit = {}

  override def pollingLimitExceeded(mediaFireFileUpload: MediaFireFileUpload): Unit = {}

  override def pollingError(mediaFireWebUpload: MediaFireWebUpload, i: Int, i1: Int, s: String): Unit = {}

  override def pollingError(mediaFireFileUpload: MediaFireFileUpload, i: Int, i1: Int, i2: Int, s: String): Unit = {}

  override def apiError(mediaFireFileUpload: MediaFireFileUpload, mediaFireApiResponse: MediaFireApiResponse): Unit = {
    finished = true
  }

  override def getNextUpload: MediaFireUpload = {null}

  override def sdkException(mediaFireWebUpload: MediaFireWebUpload, e: MediaFireException): Unit = {
    finished = true
  }

  override def sdkException(mediaFireFileUpload: MediaFireFileUpload, e: MediaFireException): Unit = {
    finished = true
  }

  override def pollingInterrupted(mediaFireWebUpload: MediaFireWebUpload, e: InterruptedException): Unit = {}

  override def pollingInterrupted(mediaFireFileUpload: MediaFireFileUpload, e: InterruptedException): Unit = {}

  override def fileIOException(mediaFireFileUpload: MediaFireFileUpload, e: IOException): Unit = {}
}