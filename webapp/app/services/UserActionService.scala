package services

import com.mohiva.play.silhouette.api.LoginInfo
import models.UserAction

import scala.concurrent.Future

/**
  * Created by akirichkov on 09.08.16.
  */
trait UserActionService {

  def saveAction(action : UserAction)

  def getLastActions(loginInfo: LoginInfo, amount: Int): Future[List[UserAction]]

}
