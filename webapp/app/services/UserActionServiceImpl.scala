package services

import play.modules.reactivemongo.ReactiveMongoPlugin
import play.modules.reactivemongo.json.collection.JSONCollection
import com.mohiva.play.silhouette.api.LoginInfo
import models.{ActionType, User, UserAction}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.Json
import play.modules.reactivemongo.ReactiveMongoPlugin
import play.modules.reactivemongo.json._
import play.modules.reactivemongo.json.collection.{JSONCollection, _}
import reactivemongo.api.Cursor
import reactivemongo.api.commands.{UpdateWriteResult, WriteResult}
import play.api.Play.current
import services.UserActionServiceImpl._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable
import scala.concurrent.Future

/**
  * Created by akirichkov on 06.08.16.
  */
class UserActionServiceImpl extends UserActionService {
  private final val logger: Logger = LoggerFactory.getLogger(classOf[UserActionServiceImpl])
  private final val AMOUNTLASTACTIONS: Int = 100

  def saveAction(action: UserAction) = {
    coll.insert(action).map {
      lastError =>
        if (lastError.hasErrors) {
          logger.error(s"Error with inserted user action : $action with LastError: $lastError", lastError.getCause)
        } else {
          logger.info(s"Successfully inserted user action : $action with LastError: $lastError")
        }
    }
  }

  def getLastActions(loginInfo: LoginInfo, amount: Int): Future[List[UserAction]] = {
    val userActionsSelector = Json.obj("loginInfo" -> loginInfo,
      "actionType" -> Json.obj("$in" -> Json.arr(ActionType.Reply, ActionType.CreateMessage, ActionType.Like)))

    val cursor1: Cursor[UserAction] = coll.find(userActionsSelector).
      sort(Json.obj("created" -> -1)).
      cursor[UserAction]
    cursor1.collect[List](AMOUNTLASTACTIONS, false)
  }
}

object UserActionServiceImpl {

  /**
    * The list of user actions.
    */
  lazy val db = ReactiveMongoPlugin.db
  lazy val coll = db.collection[JSONCollection]("actions")
}
