package models

import java.util.{Date, UUID}

import com.mohiva.play.silhouette.api.{Identity, LoginInfo}
import play.api.libs.json.Json

/**
 * The feedback object.
 *
 * @param loginInfo The linked login info.
 */
case class Feedback(
                     var loginInfo: Option[LoginInfo],
                     qid: String,
                     var created: Date,
                     loc: Map[String, Double],
                     positive: Option[Boolean],
                     var answers: Map[String, Boolean])

/**
 * The companion object.
 */
object Feedback {

  /**
   * Converts the [Feedback] object to Json and vice versa.
   */
  implicit val JsonFeedbackFormats = Json.format[Feedback]
}
