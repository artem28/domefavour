package models

import java.util.Date

import com.mohiva.play.silhouette.api.LoginInfo
import play.api.libs.json._

/**
  * Created by akirichkov on 06.08.16.
  */
case class UserAction(
                       user: LoginInfo,
                       actionType: ActionType.Value,
                       var qid: Option[String],
                       var aid: Option[String],
                       var created: Date
                     )

object ActionType extends Enumeration {
  type ActionType = Value

  val CreateMessage = Value("CreateMessage")
  val Reply = Value("Reply")
  val Login = Value("Login")
  val View = Value("View")
  val Like = Value("Like")

  implicit val myEnumFormat = new Format[ActionType] {
    //def reads(json: JsValue) = ActionType.withName(json.as[String].value) // doesn't compile
    def reads(json: JsValue) = JsSuccess(ActionType.withName(json.as[String]))
    def writes(myEnum: ActionType) = JsString(myEnum.toString)
  }
}


object UserAction {
  /**
    * Converts the [UserAction] object to Json and vice versa.
    */
  implicit val JsonUserActionFormats = Json.format[UserAction]
}
