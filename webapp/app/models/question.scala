package models

import java.util.Date

import com.mohiva.play.silhouette.api.LoginInfo

case class Question( var qid: String,
                     var user: LoginInfo,
                     var userName: String,
                     var rating: Int,
                     var badRating: Int,
                     var rel: Long,
                     text: String,
                     loc: Map[String, Double],
                     lng: Double,
                     lat: Double,
                     var answers: Array[Answer],
                     var created: Date,
                     var files: Array[FileDetails])

case class Answer(   qid: String,
                     var user: LoginInfo,
                     var userName: String,
                     var aid: String,
                     var rating: Int,
                     var badRating: Int,
                     var created: Date,
                     text: String,
                     var files: Array[FileDetails])

case class FileDetails(   fileId: String,
                     text: String,
                     contentType: String,
                     thumbId: String,
                     name: String)

object JsonQuestionFormats {
  import play.api.libs.json.Json

  implicit val fileFormat = Json.format[FileDetails]
  implicit val answerFormat = Json.format[Answer]
  // Generates Writes and Reads for Feed and User thanks to Json Macros
  implicit val questionFormat = Json.format[Question]
}

object JsonAnswerFormats {
  import play.api.libs.json.Json

  implicit val fileFormat = Json.format[FileDetails]
  // Generates Writes and Reads for Feed and User thanks to Json Macros
  implicit val answerFormat = Json.format[Answer]
}

object JsonFileFormats {
  import play.api.libs.json.Json

  // Generates Writes and Reads for Feed and User thanks to Json Macros
  implicit val fileFormat = Json.format[FileDetails]
}