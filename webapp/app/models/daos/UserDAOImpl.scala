package models.daos

import java.util.UUID

import com.mohiva.play.silhouette.api.LoginInfo
import models.User
import models.daos.UserDAOImpl._
import org.slf4j.{LoggerFactory, Logger}
import play.api.libs.json.Json
import play.modules.reactivemongo.ReactiveMongoPlugin
import play.modules.reactivemongo.json._
import play.modules.reactivemongo.json.collection.{JSONCollection, _}
import reactivemongo.api.Cursor
import reactivemongo.api.commands.{UpdateWriteResult, WriteResult}

import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global

import scala.collection.mutable
import scala.concurrent.Future

/**
 * Give access to the user object.
 */
class UserDAOImpl extends UserDAO {

  private final val logger: Logger = LoggerFactory.getLogger(classOf[UserDAOImpl])

  /**
   * Finds a user by its login info.
   *
   * @param loginInfo The login info of the user to find.
   * @return The found user or None if no user for the given login info could be found.
   */
  def find2(loginInfo: LoginInfo) = {
    Future.successful(
      users.find { case (id, user) => user.loginInfo == loginInfo }.map(_._2)
    )
  }

  def find(loginInfo: LoginInfo) = {
/*      val user : Option[User] = users.find { case (id, user) => user.loginInfo == loginInfo }.map(_._2)
      if(user.isDefined) {
        return Future.successful(user)
      }*/
      val userSelector = Json.obj("loginInfo" -> loginInfo)

      val cursor1: Cursor[User] =coll.find(userSelector).cursor[User]
      cursor1.collect[List](1, false).map({col:List[User]=>
        col.headOption
      })
  }

/*
  /**
   * Finds a user by its user ID.
   *
   * @param userID The ID of the user to find.
   * @return The found user or None if no user for the given ID could be found.
   */

  def find(userID: UUID) = {
    Future.successful(users.get(userID))
  }

  def find2(userID: UUID) = {
    Future.successful(users.get(userID))
    
  }
*/

  /**
   * Saves a user.
   *
   * @param user The user to save.
   * @return The saved user.
   */
  def save2(user: User) = {
    users += (user.userID -> user)
    Future.successful(user)
  }

  def save(user: User) = {
    users += (user.userID -> user)
    val selector = Json.obj("loginInfo" -> user.loginInfo)
    val update = Json.toJson(user)
    coll.update(selector, user, upsert = true).map {
      lastError =>
        logger.error(s"Successfully updated user with LastError: $lastError")
        user
    }
  }
}

/**
 * The companion object.
 */
object UserDAOImpl {

  /**
   * The list of users.
   */
  val users: mutable.HashMap[UUID, User] = mutable.HashMap()
  lazy val db = ReactiveMongoPlugin.db
  lazy val coll = db.collection[JSONCollection]("users")
}
